﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Rozrakh2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            textBox4.ScrollBars = ScrollBars.Vertical;
        }
        Bitmap flag; GroupBox grp;
        Graphics flagGraphics;
        Font myfont = new Font("Arial", 8);
        Brush myBrush = new SolidBrush(Color.Blue);
        Pen myPen = new Pen(Color.Blue);
        Pen myPen1 = new Pen(Color.Aqua);
        double begin, end, iteration_lenght;
        int iteration_number;
        int quality_flag = 0;
        double FMaxValue;
        double FMinValue;
        double[] Xarr, Yarr;
        double[,] matrix;
        bool isDeleted = true;
        double[] polyValue = new double[4];
        TextBox txt, InputTextbox;
        Label lbl;
        Button butt;
        //(n+1)-а похідна виразу (3/32)*Math.Sin(x/2)
        private void TextBox_Key_Press(object sender, KeyPressEventArgs e)
        {
            if ((!Char.IsDigit(e.KeyChar)) && !(e.KeyChar.Equals((char)Keys.Back)))
            {
                e.Handled = true;
            }
        }
        private void TextBox_Key_Press_(object sender, KeyPressEventArgs e)
        {
            TextBox t = (TextBox)sender;
            if ((!Char.IsNumber(e.KeyChar)) && !(e.KeyChar.Equals((char)Keys.Back)) && !(e.KeyChar.Equals(',') && !Dots_Contatin(t.Text)) && !(e.KeyChar.Equals('-') && t.Text.Length == 0))
            {
                e.Handled = true;
            }
        }
        private void Create_grp()
        {
            try { grp.Dispose(); }
            catch { }
            if (!isDeleted)
            { DeleteTxtAndLbl(); }
            if (textBox2.Text.Length != 0)
            {
                int pow = Int32.Parse(textBox2.Text) + 1;
                grp = new GroupBox();
                grp.Location = new Point(0, groupBox1.Location.Y + groupBox1.Size.Height);
                grp.Name = "GroupBox";
                grp.Size = new Size(190, (pow + 1) * 20 + 10);
                Controls.Add(grp);
                int pointX = 5, pointY = 10;
                for (int i = 0; i < 2; i++)
                {
                    lbl = new Label();
                    lbl.Name = i.ToString();
                    lbl.Location = new Point(pointX, pointY);
                    lbl.Size = new Size(40, 15);
                    switch (i)
                    {
                        case 0: lbl.Text = "x"; break;
                        case 1: lbl.Text = "f(x)"; break;
                    }
                    grp.Controls.Add(lbl);
                    pointX += lbl.Size.Width;
                }
                pointY += 15; pointX = 5;
                for (int i = 0; i < pow; i++)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        txt = new TextBox();
                        txt.Name = i.ToString() + j.ToString();
                        txt.Location = new Point(pointX, pointY);
                        txt.Size = new Size(40, 20);
                        txt.ReadOnly = true;
                        grp.Controls.Add(txt);
                        pointX += txt.Size.Width;
                    }
                    pointY += 20; pointX = 5;
                }
                pointY = 10; pointX += 90;
                lbl = new Label();
                lbl.Name = "3";
                lbl.Text = "Enter values:";
                lbl.Size = new Size(80, 15);
                lbl.Location = new Point(pointX, pointY);
                grp.Controls.Add(lbl);
                pointY += 15;
                for (int i = 0; i < 4; i++)
                {
                    InputTextbox = new TextBox();
                    InputTextbox.Name = "Input" + i.ToString();
                    InputTextbox.Location = new Point(pointX, pointY);
                    InputTextbox.Size = new Size(80, 20);
                    InputTextbox.TextChanged += new EventHandler(Data_TextChanged);
                    grp.Controls.Add(InputTextbox);
                    pointY += 20;
                }
                butt = new Button();
                butt.Name = "Calculate";
                butt.Location = new Point(pointX, pointY);
                butt.Size = new Size(80, 20);
                butt.Text = "Calculate";
                butt.Enabled = false;
                butt.Click += new EventHandler(Calculate_Click);
                grp.Controls.Add(butt);

                isDeleted = false;
            }
        }
        private void Data_TextChanged(object sender, EventArgs e)
        {
            int flag = 0;
            foreach (Control txt in grp.Controls)
            {
                if (txt.Text != "")
                {
                    flag++;
                }
            }
            if (flag == grp.Controls.Count)
            { butt.Enabled = true; }
            else butt.Enabled = false;
        }
        private void DeleteTxtAndLbl()
        {
            foreach (Control ctn in grp.Controls)
            {
                ctn.Dispose();
            }
            isDeleted = true;
        }
        private void Calculate_Click(object sender, EventArgs e)
        {
            textBox4.Clear();
            int n = Int32.Parse(textBox2.Text) + 1;
            double[,] matrix = new double[n, n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    matrix[i, j] = 0;
                }
            }
            double[] ArrX = new double[n];
            double[] ArrY = new double[n];
            double[] ArrOfKoef = new double[n];
            for (int i = 0; i < n; i++)
            {
                ArrX[i] = double.Parse(grp.Controls[i.ToString() + '0'].Text);
                ArrY[i] = double.Parse(grp.Controls[i.ToString() + '1'].Text);
                ArrOfKoef[i] = 0;
            }
            fill_matrix(matrix, n, ArrX, ArrY);
            for (int j = 0; j < ArrOfKoef.Length; j++)
            {
                ArrOfKoef[j] = matrix[0, j];
            }
            textBox4.Text += "Інтерполяційний многочлен у схемі Ньютона:";
            textBox4.Text += Environment.NewLine;
            polinomOutput(matrix, n, ArrX);
            textBox4.Text += Environment.NewLine + "Максимальне значення похибки: " + ErrorMaxValue();
            for (int i = 0; i < 4; i++)
            {
                if (grp.Controls["Input" + i.ToString()].Text != "")
                {
                    polyValue[i] = double.Parse(grp.Controls["Input" + i.ToString()].Text);
                }
                else
                {
                    MessageBox.Show("Enter data!");
                }
                textBox4.Text += Environment.NewLine + "x =" + polyValue[i] + " => L_" + n.ToString() + " =" + (return_res(polyValue[i], ArrX, matrix, n));
                textBox4.Text += "\tf(x) =" + (3 * Math.Sin(polyValue[i] + Math.PI / 4)).ToString();
                textBox4.Text += "\tПрактична похибка: " + Math.Abs(3 * Math.Sin(polyValue[i] + Math.PI / 4) - return_res(polyValue[i], ArrX, matrix, n));
                textBox4.Text += "\tТеоретична похибка: " + TheoreticalError(polyValue[i]);
            }

        }
        private void fill_data()
        {
            textBox4.Text = "";
            dataGridView1.Columns.Clear();
            int n = Int32.Parse(textBox2.Text) + 1;
            double[,] matrix = new double[n, n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    matrix[i, j] = 0;
                }
            }
            double[] ArrX = new double[n];
            double[] ArrY = new double[n];
            for (int i = 0; i < n; i++)
            {
                ArrX[i] = double.Parse(grp.Controls[i.ToString() + '0'].Text);
                ArrY[i] = double.Parse(grp.Controls[i.ToString() + '1'].Text);
            }
            fill_matrix(matrix, n, ArrX, ArrY);

            dataGridView1.ReadOnly = true;
            for (int i = 0; i < n; i++)
            {
                dataGridView1.Columns.Add(i.ToString(), i.ToString());
                dataGridView1.Columns[i].Width = dataGridView1.Width / n;
            }
            dataGridView1.Rows.Add(n);
            for (int i = 0; i < n; i++)
            {
                dataGridView1.Rows[i].Height = dataGridView1.Width / (n + 1);
            }
            dataGridView1.RowHeadersVisible = false;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (matrix[j, i] != 0) dataGridView1[i, j].Value = matrix[j, i];
                }
            }
            textBox4.Text += "Інтерполяційний многочлен у схемі Ньютона:";
            textBox4.Text += Environment.NewLine;
            polinomOutput(matrix, n, ArrX);
            textBox4.Text += Environment.NewLine + "Максимальне значення похибки: " + ErrorMaxValue();
        }
        public void fill_matrix(double[,] matrix, int size, double[] Xarr, double[] Yarr)//Побудова таблиці розділених різниць
        {
            for (int i = 0; i < size; i++)
            {
                matrix[i, 0] = Yarr[i];
            }

            int y = size;

            for (int i = 1, r = 0; i < size; i++, r++)
            {
                for (int k = 0; k < y - 1; k++)
                {
                    double ret = (matrix[k + 1, i - 1] - matrix[k, i - 1]) / (Xarr[k + r + 1] - Xarr[k]);
                    matrix[k, i] = ret;
                }
                y--;
            }
        }
        public double return_res(double x, double[] Xarr, double[,] matrix, int n)//Обчислення полінома в точці
        {
            double res = matrix[0, n - 1];
            for (int i = n - 1; i > 0; i--)
            {
                res *= (x - Xarr[i - 1]);
                res += matrix[0, i - 1];
            }
            return res;
        }
        public void polinomOutput(double[,] matrix, int size, double[] Xarr)//вивід полінома Ньютона
        {
            for (int i = 0; i < size-1; i++)
            {
                if (i != 0) textBox4.Text += "(";
                textBox4.Text += Math.Round(matrix[0, i], 4).ToString() + " + (x - " + Math.Round(Xarr[i], 4) + ")";
            }
            for (int i = 0; i < size-1; i++)
            {
                textBox4.Text += ")";
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            checkBox1.Enabled = true;
            FMaxValue = FMinValue = 0;
            if (Double.TryParse(textBox1.Text, out begin) && Double.TryParse(textBox3.Text, out end) && Int32.TryParse(textBox2.Text, out iteration_number))
            {
                if (!ambit_check())
                {
                    MessageBox.Show("Please, enter correct data!");
                    checkBox1.Enabled = false;
                    return;
                }
                try
                {
                    begin = double.Parse(textBox1.Text);
                    end = double.Parse(textBox3.Text);
                    iteration_number = int.Parse(textBox2.Text);
                    iteration_lenght = Math.Abs(begin - end) / iteration_number;
                    Xarr = new double[iteration_number + 1];
                    Yarr = new double[iteration_number + 1];
                    matrix = new double[iteration_number + 1, iteration_number + 1];
                    Create_grp();
                    dataGridView1.RowHeadersVisible = false;
                    double x = begin;
                    int j = 0;
                    for (int i = 0; i <= iteration_number; i++)
                    {
                        Xarr[i] = x;
                        grp.Controls[j.ToString() + "0"].Text = x.ToString();
                        Yarr[i] = (3 * Math.Sin(x + Math.PI / 4));
                        grp.Controls[j.ToString() + "1"].Text = (3 * Math.Sin(x + Math.PI / 4)).ToString();
                        x += iteration_lenght; j++;
                    }
                }
                catch { }
                MaxValueDX();
                fill_data();
                MaxValue();
                MinValue();
                MathGraph();
            }
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            { checkBox1.Text = "Turn Off(Net)"; }
            else
            {
                checkBox1.Text = "Turn On(Net)";
            }
            MathGraph();
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            checkBox1.Enabled = false;
            if (string.IsNullOrEmpty(textBox1.Text) || string.IsNullOrEmpty(textBox2.Text) || string.IsNullOrEmpty(textBox3.Text))
            {
                button1.Enabled = false;
            }
            else { button1.Enabled = true; }
        }
        private void MathGraph()
        {
            int wX;
            int hX;
            int kX = quality_identifier(Math.Abs(double.Parse(textBox1.Text) - double.Parse(textBox3.Text)), 0);
            int kY = quality_identifier(Math.Abs(FMaxValue - FMinValue), 0);
            double xF, yF;
            double x;

            wX = pictureBox1.Width;
            hX = pictureBox1.Height;

            double masX = wX / (Math.Abs(double.Parse(textBox1.Text) - double.Parse(textBox3.Text)));
            double masY = hX / ((FMaxValue - FMinValue) * 2);
            double stepX = (Math.Abs(double.Parse(textBox1.Text) - double.Parse(textBox3.Text))) / wX;
            int Line_x = (int)(wX / 2 - masX * (double.Parse(textBox1.Text) + double.Parse(textBox3.Text)) / 2);
            int Line_y = (int)(hX / 2 + masY * (FMaxValue + FMinValue) / 2);

            flag = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            flagGraphics = Graphics.FromImage(flag);
            flagGraphics.Clear(pictureBox1.BackColor);

            LineDraw(Line_x, Line_y, wX, hX, masX, masY, kX, kY);

            flagGraphics.DrawLine(myPen, 0, Line_y, wX, Line_y);//Horizontal
            flagGraphics.DrawLine(myPen, Line_x, 0, Line_x, hX);//Vertical


            for (x = double.Parse(textBox1.Text); x < double.Parse(textBox3.Text); x += stepX / masY)
            {
                xF = (x * masX) + Line_x;
                double tmp = ((3 * Math.Sin(x + Math.PI / 4))) * (masY);
                yF = Line_y - tmp;
                try
                {
                    flag.SetPixel((int)xF, (int)yF, Color.Red);
                }
                catch (Exception)
                { }
            }
            pictureBox1.Image = flag;

        }
        private void LineDraw(int Line_x, int Line_y, int wX, int hX, double masX, double masY, int kX, int kY)
        {
            double koefX = Math.Pow(10, quality_flag * kX);
            double koefY = Math.Pow(10, quality_flag * kY);
            double mark = 0;
            for (double i = Line_x; i < wX; i += masX / koefX)
            {
                if (checkBox1.Checked)
                {
                    flagGraphics.DrawLine(myPen1, (float)i, 0, (float)i, hX);
                }
                flagGraphics.DrawLine(myPen, (float)i, (float)(Line_y - 3), (float)i, (float)(Line_y + 3));
                flagGraphics.DrawString((mark / koefX).ToString(), myfont, myBrush, (float)i, (float)(Line_y - 2));
                mark += 1;
            }
            mark = 0;
            for (double i = Line_x; i > 0; i -= masX / koefX)
            {
                if (checkBox1.Checked)
                {
                    flagGraphics.DrawLine(myPen1, (float)i, 0, (float)i, hX);
                }
                flagGraphics.DrawLine(myPen, (float)i, (float)(Line_y - 3), (float)i, (float)(Line_y + 3));
                flagGraphics.DrawString((-mark / koefX).ToString(), myfont, myBrush, (float)i, (float)(Line_y - 2));
                mark++;
            }
            mark = 0;
            for (double i = Line_y; i < hX; i += masY / koefY)
            {
                if (checkBox1.Checked)
                {
                    flagGraphics.DrawLine(myPen1, 0, (float)i, wX, (float)i);
                }
                flagGraphics.DrawLine(myPen, (float)(Line_x - 3), (float)i, (float)(Line_x + 3), (float)i);
                flagGraphics.DrawString((-mark / koefY).ToString(), myfont, myBrush, (float)(Line_x), (float)i - 2);
                mark++;
            }
            mark = 0;
            for (double i = Line_y; i > 0; i -= masY / koefY)
            {
                if (checkBox1.Checked)
                {
                    flagGraphics.DrawLine(myPen1, 0, (float)i, wX, (float)i);
                }
                flagGraphics.DrawLine(myPen, (float)(Line_x - 3), (float)i, (float)(Line_x + 3), (float)i);
                flagGraphics.DrawString((mark / koefY).ToString(), myfont, myBrush, (float)(Line_x), (float)i - 2);
                mark++;
            }
        }
        private void MaxValue()
        {
            double step_ = (Math.Abs(double.Parse(textBox1.Text) - double.Parse(textBox3.Text))) / pictureBox1.Width;
            for (double x = double.Parse(textBox1.Text); x < double.Parse(textBox3.Text); x += step_)
            {
                double f = 3 * Math.Sin(x + Math.PI/4);
                if (f > FMaxValue)
                {
                    FMaxValue = f;
                }
            }
        }
        private void MinValue()
        {
            double step_ = (Math.Abs(double.Parse(textBox1.Text) - double.Parse(textBox3.Text))) / pictureBox1.Width;

            for (double x = double.Parse(textBox1.Text); x < double.Parse(textBox3.Text); x += step_)
            {
                double f = 3 * Math.Sin(x + Math.PI / 4);
                if (f < FMinValue)
                {
                    FMinValue = f;
                }
            }
        }
        private bool ambit_check()
        {
            if (Double.TryParse(textBox1.Text, out begin) && Double.TryParse(textBox3.Text, out end))
            {
                double x1, x2;
                x1 = double.Parse(textBox1.Text);
                x2 = double.Parse(textBox3.Text);
                if (x1 >= x2)
                { return false; }
            }
            return true;
        }
        private bool Dots_Contatin(string s)
        {
            foreach (char a in s)
            {
                if (a.Equals(','))
                { return true; }
            }
            return false;
        }
        private int quality_identifier(double number, int k)
        {
            if (Math.Abs(number) <= Math.Pow(10, -k))
            {
                k = quality_identifier(Math.Abs(number), k + 1);
                quality_flag = 1;
                return k;
            }
            else if (Math.Abs(number) >= Math.Pow(10, k + 1))
            {
                k = quality_identifier(Math.Abs(number), k + 1);
                quality_flag = -1;
                return k;
            }
            return k;
        }
        private double MaxValueDX()
        {
            double step_ = (Math.Abs(double.Parse(textBox1.Text) - double.Parse(textBox3.Text))) / pictureBox1.Width;
            double FMaxValueDX = 0;
            for (double x = double.Parse(textBox1.Text); x < double.Parse(textBox3.Text); x += step_)
            {
                double f = 3 * Math.Sin(x + Math.PI/4);
                if (f > FMaxValueDX)
                {
                    FMaxValueDX = f;
                }
            }
            return FMaxValueDX;
        }
        private double TheoreticalError(double x)
        {
            double w = 1; int i = 1;
            foreach (double t in Xarr)
            {
                w *= (x - t) / i; i++;
            }
            return Math.Abs(MaxValueDX() * w);
        }
        private double ErrorMaxValue()
        {
            double step_ = (Math.Abs(double.Parse(textBox1.Text) - double.Parse(textBox3.Text))) / pictureBox1.Width;
            double ErrorMaxValue = 0;
            for (double x = double.Parse(textBox1.Text); x < double.Parse(textBox3.Text); x += step_)
            {
                double f = TheoreticalError(x);
                if (f > ErrorMaxValue)
                {
                    ErrorMaxValue = f;
                }
            }
            return ErrorMaxValue;
        }
    }
}
